<?php

namespace App;

class Accounting
{
    private $repo;
    private $currentData;

    public function __construct(IBudgetRepo $budgetRepo)
    {
        $this->repo = $budgetRepo;
    }

    public function queryBudget(\DateTime $start, \DateTime $end)
    {
        $this->currentData = $this->repo->getAll();

        $total = 0;
        if ($start->format('Ym') == $end->format('Ym')) {
            $amount = $this->getAmount($start->format('Ym'));
            $total += $amount * $this->availableFraction($start, $end);
        } elseif ($end > $start) {
            $amount = $this->getAmount($start->format('Ym'));
            $endDayOfTheMonth = date_create_from_format('Y-m-d', $start->format('Y-m-t'));
            $total += $amount * $this->availableFraction($start, $endDayOfTheMonth);

            $amount = $this->getAmount($end->format('Ym'));
            $firstDayOfTheMonth = date_create_from_format('Y-m-d', $end->format('Y-m') . '-01');
            $total += $amount * $this->availableFraction($firstDayOfTheMonth, $end);
        }

        $period = $this->getMiddleMonthesPeriod($start, $end);
        foreach ($period as $dt) {
            $amount = $this->getAmount($dt->format('Ym'));
            $total += $amount;
        }

        return $total;
    }

    private function getMiddleMonthesPeriod(\DateTime $start, \DateTime $end)
    {
        $firstDayOfStartMonth = date_create_from_format('Y-m-d', $start->format('Y-m') . "-01");
        $firstDayOfStartMonth->modify('+1 month');
        $firstDayOfEndMonth = date_create_from_format('Y-m-d', $end->format('Y-m') . "-01");
        $interval = new \DateInterval('P1M');

        return new \DatePeriod($firstDayOfStartMonth, $interval, $firstDayOfEndMonth);
    }

    private function availableFraction(\DateTime $start, \DateTime $end)
    {
        $focusMonth = ($start) ? $start : $end;
        $actualDays = $focusMonth->format('t');
        $calculatedDays = intval($end->format('d')) - intval($start->format('d')) + 1;

        return $calculatedDays / $actualDays;
    }

    private function getAmount(string $ym)
    {
        foreach ($this->currentData as $data) {
            if ($data->yearMonth == $ym) {
                return $data->amount;
            }
        }

        return 0;
    }
}
